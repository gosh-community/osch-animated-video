1
00:00:01,001 --> 00:00:02,836
The climate crisis,

2
00:00:02,903 --> 00:00:04,704
pollution, droughts,

3
00:00:04,704 --> 00:00:05,671
pandemics,

4
00:00:06,072 --> 00:00:08,140
poverty, inequality

5
00:00:08,140 --> 00:00:10,242
loss of biodiversity...

6
00:00:10,242 --> 00:00:15,127
the 21st century has plenty of challenges for humanity.

7
00:00:15,127 --> 00:00:18,000
We have always overcome challenges

8
00:00:18,000 --> 00:00:20,733
thanks to our capacity for innovation.

9
00:00:20,733 --> 00:00:22,786
The knowledge we have today

10
00:00:22,786 --> 00:00:26,535
has been transmitted from generation to generation: 

11
00:00:26,535 --> 00:00:29,849
that is culture, including science.

12
00:00:29,849 --> 00:00:31,387
This evolution

13
00:00:31,387 --> 00:00:34,177
has never been the fruit of a single person,

14
00:00:34,177 --> 00:00:36,784
but the result of a collective effort.

15
00:00:36,784 --> 00:00:40,832
We are not the strongest or fastest species,

16
00:00:40,832 --> 00:00:43,765
but the one who has learned to collaborate

17
00:00:43,765 --> 00:00:46,792
and think together to solve complex problems. 

18
00:00:47,000 --> 00:00:49,082
We need more knowledge,

19
00:00:49,082 --> 00:00:49,941
and to produce it,

20
00:00:50,000 --> 00:00:51,847
we need more science.

21
00:00:51,847 --> 00:00:53,550
But science done where,

22
00:00:53,550 --> 00:00:54,467
by whom,

23
00:00:54,467 --> 00:00:56,587
and following which interests?

24
00:00:57,259 --> 00:01:00,190
A recent global study shows that 90 percent

25
00:01:00,190 --> 00:01:02,188
of new scientific knowledge

26
00:01:02,188 --> 00:01:04,613
is produced in  developed countries,

27
00:01:04,613 --> 00:01:06,785
and less than 40 percent

28
00:01:06,788 --> 00:01:09,005
is dedicated to solving problems

29
00:01:09,005 --> 00:01:12,760
enshrined in the UN Sustainable Development Goals -

30
00:01:12,760 --> 00:01:17,813
such as hunger, lack of clean water or affordable energy.

31
00:01:18,180 --> 00:01:20,018
We face a dilemma:

32
00:01:20,018 --> 00:01:22,303
The majority of scientific outputs

33
00:01:22,303 --> 00:01:26,318
are not directed towards the most urgent challenges,

34
00:01:26,319 --> 00:01:31,561
and those seeking to use science to address these challenges face barriers.

35
00:01:31,868 --> 00:01:34,911
We need to democratize science.

36
00:01:34,911 --> 00:01:39,012
But scientific research requires increasingly complex tools:

37
00:01:39,528 --> 00:01:42,296
like microscopes, chemical reagents,

38
00:01:42,296 --> 00:01:44,135
environmental monitors,

39
00:01:44,135 --> 00:01:45,412
drones, computers,

40
00:01:45,412 --> 00:01:48,312
and many other precision instruments.

41
00:01:48,312 --> 00:01:50,129
They are often expensive

42
00:01:50,146 --> 00:01:52,503
and hard to find and maintain.

43
00:01:53,039 --> 00:01:54,363
With this in mind,

44
00:01:54,363 --> 00:01:56,257
a growing global movement

45
00:01:56,257 --> 00:01:58,802
is working to open up scientific equipment:

46
00:01:58,802 --> 00:02:02,511
that is, to share the knowledge already created.

47
00:02:02,511 --> 00:02:07,116
Open hardware is based on the same principles as open software:

48
00:02:07,116 --> 00:02:10,729
open licenses allowing everyone to use,

49
00:02:10,729 --> 00:02:14,080
study, modify, and share designs,

50
00:02:14,232 --> 00:02:17,885
aiming to make the tools they need at a lower cost.

51
00:02:18,820 --> 00:02:21,771
Simplifying access to scientific instruments

52
00:02:21,771 --> 00:02:24,939
enables more people to participate in science,

53
00:02:24,939 --> 00:02:28,352
and thus produce more diverse and useful knowledge.

54
00:02:28,352 --> 00:02:32,413
It also facilitates science education, in a virtuous circle.

55
00:02:33,797 --> 00:02:37,659
To this end, the global open science hardware movement

56
00:02:37,659 --> 00:02:42,541
promotes the open publication of designs with complete documentation,

57
00:02:42,541 --> 00:02:46,058
along with support materials such as tutorials.

58
00:02:46,683 --> 00:02:49,996
Forums, workshops and face-to-face meetings

59
00:02:50,000 --> 00:02:54,094
also help ease the learning process of assembling the tools.

60
00:02:54,428 --> 00:02:58,068
Low-cost electronic components and 3D printing

61
00:02:58,068 --> 00:03:01,419
accelerate the multiplication of open hardware.

62
00:03:01,699 --> 00:03:04,029
Easy to assemble, DIY tools

63
00:03:04,029 --> 00:03:10,043
are very useful in emergencies or in places where the latest technology is not available.

64
00:03:10,285 --> 00:03:16,002
In addition, open instruments are more flexible than proprietary products,

65
00:03:16,002 --> 00:03:22,068
and thus allow communities to empower themselves and carry out their own research:

66
00:03:22,068 --> 00:03:25,681
they open up the agenda of knowledge production.

67
00:03:26,664 --> 00:03:31,379
Times of crisis call for greater and more diverse participation,

68
00:03:31,379 --> 00:03:35,898
collective knowledge and technological sovereignty:

69
00:03:35,898 --> 00:03:38,342
a project for every problem,

70
00:03:38,342 --> 00:03:40,856
a tool for every research team.

71
00:03:41,080 --> 00:03:42,475
Open your toolkit,

72
00:03:42,475 --> 00:03:46,355
and join the Global Open Science Hardware community.

