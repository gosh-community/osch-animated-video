1
00:00:00,914 --> 00:00:04,326
La crise climatique, la pollution, les sécheresses,

2
00:00:04,326 --> 00:00:06,326
les pandémies, la pauvreté, 

3
00:00:06,326 --> 00:00:09,744
les inégalités, la perte de biodiversité

4
00:00:10,259 --> 00:00:14,423
des défis parmi tant d'autres du XXIe siècle. 

5
00:00:15,145 --> 00:00:17,430
Nous avons toujours relevé les défis

6
00:00:17,430 --> 00:00:20,169
grâce à notre capacité d'innovation.

7
00:00:20,302 --> 00:00:22,910
Le savoir dont nous disposons aujourd'hui 

8
00:00:22,910 --> 00:00:25,477
a été transmis de génération en génération:

9
00:00:25,643 --> 00:00:28,299
c'est la culture, notamment la science.

10
00:00:29,864 --> 00:00:33,081
Cette évolution n'a jamais été le fruit d'une seule personne,

11
00:00:33,081 --> 00:00:35,515
mais le résultat d'un effort collectif.

12
00:00:36,000 --> 00:00:39,340
Nous ne sommes pas l'espèce la plus forte ou la plus rapide,

13
00:00:39,340 --> 00:00:43,617
mais celle qui a appris à collaborer et à réfléchir ensemble

14
00:00:43,733 --> 00:00:45,733
pour résoudre des problèmes complexes. 

15
00:00:47,000 --> 00:00:49,694
Nous avons besoin de plus de connaissances,

16
00:00:49,694 --> 00:00:50,773
et pour les produire, 

17
00:00:50,773 --> 00:00:53,462
nous avons besoin de plus de travaux scientifiques.

18
00:00:53,545 --> 00:00:55,041
Mais une science faite où,

19
00:00:55,240 --> 00:00:56,062
par qui, 

20
00:00:56,394 --> 00:00:58,209
et répondant à quels enjeux?

21
00:00:58,209 --> 00:01:02,940
Une étude récente montre que 90 % des nouvelles connaissances scientifiques

22
00:01:02,940 --> 00:01:05,349
sont produites dans les pays développés

23
00:01:05,349 --> 00:01:08,614
et que moins de 40 % d'entre elles sont consacrées

24
00:01:08,614 --> 00:01:13,942
à la résolution des problèmes inscrits dans les objectifs de développement durable des Nations unies,

25
00:01:13,942 --> 00:01:14,946
tels que la faim,

26
00:01:14,946 --> 00:01:17,516
le manque d'eau potable ou d'énergie abordable.

27
00:01:17,698 --> 00:01:19,781
Nous sommes confrontés à un dilemme:

28
00:01:19,781 --> 00:01:24,458
la majorité des résultats scientifiques ne sont pas orientés vers les défis les plus urgents,

29
00:01:24,458 --> 00:01:30,542
et ceux qui cherchent à se servir de la science pour relever ces défis se heurtent à des obstacles. 

30
00:01:31,148 --> 00:01:33,328
Nous devons démocratiser la science. 

31
00:01:33,942 --> 00:01:38,174
Mais la recherche scientifique nécessite des outils de plus en plus sophistiqués:

32
00:01:38,880 --> 00:01:41,360
microscopes, réactifs chimiques, 

33
00:01:41,684 --> 00:01:43,287
moniteurs environnementaux,

34
00:01:43,727 --> 00:01:47,645
drones, ordinateurs et bien d'autres instruments de précision.

35
00:01:47,645 --> 00:01:52,000
Ils sont souvent coûteux et difficiles à acquérir et à entretenir. 

36
00:01:52,590 --> 00:01:56,094
C'est dans cette perspective qu'un mouvement mondial grandissant

37
00:01:56,094 --> 00:01:58,866
œuvre à la libéralisation du matériel scientifique,

38
00:01:58,991 --> 00:02:01,282
en partageant les découvertes déjà faites.

39
00:02:01,514 --> 00:02:03,450
Le matériel ouvert/Open Hardware 

40
00:02:03,450 --> 00:02:07,334
repose sur les mêmes principes que les logiciels ouverts:

41
00:02:07,334 --> 00:02:11,018
des licences ouvertes permettant à chacun d'utiliser,

42
00:02:11,018 --> 00:02:14,736
d'étudier, de modifier et de partager des modèles, 

43
00:02:14,736 --> 00:02:18,901
dans le but de fabriquer les outils dont ils ont besoin à moindre coût. 

44
00:02:18,901 --> 00:02:21,590
La simplification de l'accès aux outils scientifiques

45
00:02:21,881 --> 00:02:25,814
permet à un plus grand nombre de personnes de contribuer à la science,

46
00:02:25,814 --> 00:02:29,255
et donc de produire des connaissances diversifiées et utiles. 

47
00:02:29,255 --> 00:02:31,897
Elle facilite également l'enseignement des sciences,

48
00:02:31,897 --> 00:02:33,449
dans un cercle vertueux.

49
00:02:33,449 --> 00:02:36,809
Le mouvement mondial en faveur du matériel scientifique ouvert

50
00:02:36,809 --> 00:02:40,266
encourage à cette fin la publication ouverte de conceptions

51
00:02:40,266 --> 00:02:42,681
accompagnées d'une documentation complète, 

52
00:02:42,681 --> 00:02:46,432
ainsi que de matériel de soutien tel que des didacticiels. 

53
00:02:46,640 --> 00:02:50,123
Les forums, les ateliers et les rassemblements en présentiel 

54
00:02:50,123 --> 00:02:55,689
contribuent également à faciliter le processus d'apprentissage de l'assemblage des outils. 

55
00:02:55,689 --> 00:02:59,840
Les composants électroniques à moindre coût et l'impression 3D 

56
00:02:59,840 --> 00:03:02,773
accélèrent la prolifération du matériel ouvert.

57
00:03:02,973 --> 00:03:07,553
 Faciles à assembler, les outils de bricolage sont très utiles en cas d'urgence

58
00:03:07,752 --> 00:03:11,634
ou dans des endroits où les dernières technologies ne sont pas disponibles.

59
00:03:12,134 --> 00:03:15,299
En outre, les matériels ouverts (Open hardwares)

60
00:03:15,780 --> 00:03:18,644
sont plus flexibles que les produits propriétaires,

61
00:03:18,644 --> 00:03:21,706
et permettent donc aux communautés de s'autonomiser 

62
00:03:21,706 --> 00:03:23,706
et de mener leurs propres recherches: 

63
00:03:23,706 --> 00:03:26,771
ils ouvrent l'agenda de la production de connaissances. 

64
00:03:26,945 --> 00:03:29,601
Les périodes de crise appellent une participation

65
00:03:29,601 --> 00:03:31,743
plus importante et plus diversifiée, 

66
00:03:31,743 --> 00:03:35,045
des connaissances collectives et une souveraineté technologique:

67
00:03:35,319 --> 00:03:37,143
un projet pour chaque problème,

68
00:03:37,143 --> 00:03:39,843
un outil pour chaque équipe de recherche.

69
00:03:40,075 --> 00:03:42,075
Ouvrez votre trousse à outils,

70
00:03:42,075 --> 00:03:46,254
et rejoignez la communauté mondiale du matériel scientifique ouvert.

