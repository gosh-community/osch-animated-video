1
00:00:00,914 --> 00:00:04,326
La crise climatique, la pollution, les sécheresses,

2
00:00:04,326 --> 00:00:06,326
les pandémies, la pauvreté, 

3
00:00:06,326 --> 00:00:09,744
les inégalités, la perte de biodiversité

4
00:00:10,259 --> 00:00:14,423
des défis parmi tant d'autres du XXIe siècle. 

5
00:00:16,053 --> 00:00:18,747
Nous avons besoin de plus de connaissances,

6
00:00:18,747 --> 00:00:19,826
et pour les produire, 

7
00:00:19,826 --> 00:00:22,515
nous avons besoin de plus de travaux scientifiques.

8
00:00:22,515 --> 00:00:24,011
Mais une science faite où,

9
00:00:24,011 --> 00:00:24,833
par qui, 

10
00:00:24,833 --> 00:00:26,648
et répondant à quels enjeux?

11
00:00:26,950 --> 00:00:31,681
Une étude récente montre que 90 % des nouvelles connaissances scientifiques

12
00:00:31,681 --> 00:00:34,090
sont produites dans les pays développés

13
00:00:34,090 --> 00:00:37,355
et que moins de 40 % d'entre elles sont consacrées

14
00:00:37,355 --> 00:00:42,683
à la résolution des problèmes inscrits dans les objectifs de développement durable des Nations unies,

15
00:00:42,683 --> 00:00:43,687
tels que la faim,

16
00:00:43,687 --> 00:00:46,257
le manque d'eau potable ou d'énergie abordable.

17
00:00:46,257 --> 00:00:48,340
Nous sommes confrontés à un dilemme:

18
00:00:48,340 --> 00:00:53,017
la majorité des résultats scientifiques ne sont pas orientés vers les défis les plus urgents,

19
00:00:53,017 --> 00:00:59,101
et ceux qui cherchent à se servir de la science pour relever ces défis se heurtent à des obstacles. 

20
00:00:59,529 --> 00:01:01,709
Nous devons démocratiser la science. 

21
00:01:02,365 --> 00:01:07,111
Mais la recherche scientifique nécessite des outils de plus en plus sophistiqués:

22
00:01:07,111 --> 00:01:10,177
microscopes, réactifs chimiques, 

23
00:01:10,177 --> 00:01:11,780
moniteurs environnementaux,

24
00:01:11,780 --> 00:01:15,991
drones, ordinateurs et bien d'autres instruments de précision.

25
00:01:15,991 --> 00:01:20,834
Ils sont souvent coûteux et difficiles à acquérir et à entretenir. 

26
00:01:21,362 --> 00:01:24,866
C'est dans cette perspective qu'un mouvement mondial grandissant

27
00:01:24,866 --> 00:01:27,638
œuvre à la libéralisation du matériel scientifique,

28
00:01:27,638 --> 00:01:29,929
en partageant les découvertes déjà faites.

29
00:01:30,000 --> 00:01:31,936
Le matériel ouvert/Open Hardware 

30
00:01:31,936 --> 00:01:35,820
repose sur les mêmes principes que les logiciels ouverts:

31
00:01:35,820 --> 00:01:39,504
des licences ouvertes permettant à chacun d'utiliser,

32
00:01:39,504 --> 00:01:43,222
d'étudier, de modifier et de partager des modèles, 

33
00:01:43,222 --> 00:01:47,387
dans le but de fabriquer les outils dont ils ont besoin à moindre coût. 

34
00:01:47,387 --> 00:01:50,076
La simplification de l'accès aux outils scientifiques

35
00:01:50,076 --> 00:01:54,009
permet à un plus grand nombre de personnes de contribuer à la science,

36
00:01:54,009 --> 00:01:57,450
et donc de produire des connaissances diversifiées et utiles. 

37
00:01:57,450 --> 00:02:00,092
Elle facilite également l'enseignement des sciences,

38
00:02:00,092 --> 00:02:01,644
dans un cercle vertueux.

39
00:02:01,644 --> 00:02:05,004
Le mouvement mondial en faveur du matériel scientifique ouvert

40
00:02:05,004 --> 00:02:08,461
encourage à cette fin la publication ouverte de conceptions

41
00:02:08,461 --> 00:02:10,876
accompagnées d'une documentation complète, 

42
00:02:10,876 --> 00:02:14,627
ainsi que de matériel de soutien tel que des didacticiels. 

43
00:02:14,627 --> 00:02:18,110
Les forums, les ateliers et les rassemblements en présentiel 

44
00:02:18,110 --> 00:02:24,164
contribuent également à faciliter le processus d'apprentissage de l'assemblage des outils. 

45
00:02:24,164 --> 00:02:28,315
Les composants électroniques à moindre coût et l'impression 3D 

46
00:02:28,315 --> 00:02:30,760
accélèrent la prolifération du matériel ouvert.

47
00:02:31,549 --> 00:02:36,129
 Faciles à assembler, les outils de bricolage sont très utiles en cas d'urgence

48
00:02:36,617 --> 00:02:40,499
ou dans des endroits où les dernières technologies ne sont pas disponibles.

49
00:02:40,578 --> 00:02:43,743
En outre, les matériels ouverts (Open hardwares)

50
00:02:44,357 --> 00:02:47,221
sont plus flexibles que les produits propriétaires,

51
00:02:47,221 --> 00:02:50,283
et permettent donc aux communautés de s'autonomiser 

52
00:02:50,283 --> 00:02:52,283
et de mener leurs propres recherches: 

53
00:02:52,283 --> 00:02:55,348
ils ouvrent l'agenda de la production de connaissances. 

54
00:02:55,814 --> 00:02:58,470
Les périodes de crise appellent une participation

55
00:02:58,467 --> 00:03:00,758
plus importante et plus diversifiée, 

56
00:03:00,750 --> 00:03:03,765
des connaissances collectives et une souveraineté technologique:

57
00:03:03,765 --> 00:03:05,637
un projet pour chaque problème,

58
00:03:05,637 --> 00:03:08,420
un outil pour chaque équipe de recherche.

59
00:03:08,417 --> 00:03:10,750
Ouvrez votre trousse à outils,

60
00:03:10,750 --> 00:03:15,250
et rejoignez la communauté mondiale du matériel scientifique ouvert.

