1
00:00:01,208 --> 00:00:04,194
Crisis climática, contaminación,  sequías, 

2
00:00:04,194 --> 00:00:06,294
pandemias, pobreza,  

3
00:00:06,535 --> 00:00:07,712
desigualdad,

4
00:00:07,712 --> 00:00:09,452
pérdida de biodiversidad.

5
00:00:09,452 --> 00:00:13,953
El siglo XXI está lleno de desafíos para la humanidad.

6
00:00:15,814 --> 00:00:17,407
Siempre superamos los retos 

7
00:00:17,407 --> 00:00:19,407
gracias a nuestra capacidad de innovación.

8
00:00:20,187 --> 00:00:22,000
El conocimiento que tenemos hoy

9
00:00:22,000 --> 00:00:24,296
se transmitió de generación en generación:

10
00:00:24,296 --> 00:00:26,780
eso es la cultura, incluida la ciencia.

11
00:00:28,000 --> 00:00:31,197
Esta evolución nunca fue fruto de una sola persona,

12
00:00:31,355 --> 00:00:33,741
sino el resultado de un esfuerzo colectivo.

13
00:00:35,436 --> 00:00:38,056
No somos la especie más fuerte ni la más rápida, 

14
00:00:38,264 --> 00:00:39,898
sino la que aprendió a colaborar

15
00:00:39,898 --> 00:00:43,251
y a pensar en conjunto para resolver problemas complejos. 

16
00:00:46,957 --> 00:00:48,633
Necesitamos más conocimiento

17
00:00:48,633 --> 00:00:51,009
y para producirlo necesitamos más ciencia.

18
00:00:51,499 --> 00:00:52,986
Pero ¿ciencia hecha dónde,

19
00:00:53,327 --> 00:00:54,110
por quiénes,

20
00:00:54,393 --> 00:00:55,900
y siguiendo qué intereses?

21
00:00:56,423 --> 00:00:58,070
Un estudio mundial reciente 

22
00:00:58,070 --> 00:01:01,458
muestra que el 90 por ciento del nuevo conocimiento científico

23
00:01:01,857 --> 00:01:04,110
se produce en países desarrollados.

24
00:01:05,099 --> 00:01:06,540
Y menos del 40

25
00:01:06,540 --> 00:01:08,868
 se dedica a resolver problemas consagrados

26
00:01:08,868 --> 00:01:12,106
en los Objetivos de Desarrollo Sostenible de las Naciones Unidas,

27
00:01:12,106 --> 00:01:14,195
como el hambre, la falta de agua potable

28
00:01:14,195 --> 00:01:15,730
o de energía accesible.

29
00:01:16,985 --> 00:01:18,617
Enfrentamos un dilema: 

30
00:01:18,617 --> 00:01:20,908
la mayor parte de las investigaciones

31
00:01:20,908 --> 00:01:23,739
no están dirigidas a los desafíos más urgentes,

32
00:01:23,739 --> 00:01:26,927
y quienes buscan usar la ciencia para abordar estos desafíos

33
00:01:26,927 --> 00:01:28,927
enfrentan barreras. 

34
00:01:31,295 --> 00:01:33,791
Necesitamos democratizar la ciencia.

35
00:01:33,791 --> 00:01:37,220
Pero la investigación científica requiere herramientas 

36
00:01:37,361 --> 00:01:39,361
cada vez más complejas:

37
00:01:39,419 --> 00:01:41,796
microscopios, reactivos químicos, 

38
00:01:41,796 --> 00:01:43,563
monitores ambientales,

39
00:01:43,563 --> 00:01:46,885
drones, computadoras y muchos otros instrumentos. 

40
00:01:48,763 --> 00:01:51,744
Suelen ser caros, difíciles de encontrar y de mantener. 

41
00:01:53,422 --> 00:01:54,475
Con esto en mente,

42
00:01:54,475 --> 00:01:56,354
un creciente movimiento global

43
00:01:56,354 --> 00:01:58,740
trabaja para abrir el equipamiento científico: 

44
00:01:58,740 --> 00:02:02,446
es decir, para compartir el conocimiento que ya existe.

45
00:02:02,446 --> 00:02:04,000
El hardware abierto se basa

46
00:02:04,000 --> 00:02:06,823
en los mismos principios que el software abierto:

47
00:02:06,823 --> 00:02:10,349
licencias que permiten a cualquier persona usar,

48
00:02:10,673 --> 00:02:13,304
estudiar, modificar y compartir diseños,

49
00:02:13,711 --> 00:02:15,742
con el objetivo de fabricar las herramientas

50
00:02:15,742 --> 00:02:17,742
que necesite a un costo menor. 

51
00:02:19,047 --> 00:02:21,812
Simplificar el acceso a los instrumentos científicos

52
00:02:22,045 --> 00:02:25,206
permite que más personas participen en la ciencia

53
00:02:25,206 --> 00:02:26,213
y, por lo tanto, 

54
00:02:26,213 --> 00:02:29,478
que produzcan conocimientos más diversos y más útiles. 

55
00:02:30,151 --> 00:02:32,553
También facilita la educación científica,

56
00:02:32,836 --> 00:02:34,187
en un círculo virtuoso.

57
00:02:34,893 --> 00:02:37,816
El movimiento mundial por el hardware científico abierto

58
00:02:37,816 --> 00:02:41,729
promueve la publicación abierta de diseños con documentación completa,

59
00:02:44,000 --> 00:02:46,443
junto con materiales de apoyo como tutoriales.

60
00:02:46,900 --> 00:02:49,548
Foros, talleres y encuentros presenciales 

61
00:02:49,548 --> 00:02:52,959
también ayudan a facilitar el proceso de aprendizaje. 

62
00:02:55,020 --> 00:02:57,090
Los componentes electrónicos de bajo costo

63
00:02:57,090 --> 00:02:58,316
y la impresión 3D

64
00:02:58,499 --> 00:03:00,964
aceleran la multiplicación del hardware abierto. 

65
00:03:01,554 --> 00:03:04,806
Que casi cualquier persona puede armar las herramientas

66
00:03:04,806 --> 00:03:07,402
es algo muy útil en situaciones de emergencia,

67
00:03:07,402 --> 00:03:10,587
o cuando la última tecnología simplemente no llega.

68
00:03:11,518 --> 00:03:13,349
Además, los instrumentos abiertos 

69
00:03:13,349 --> 00:03:15,872
son más flexibles que los productos propietarios

70
00:03:16,279 --> 00:03:19,135
y, por lo tanto, ayudan a las comunidades a empoderarse

71
00:03:19,135 --> 00:03:21,338
y realizar sus propias investigaciones.

72
00:03:21,679 --> 00:03:24,790
Esto permite abrir la agenda de producción de conocimiento. 

73
00:03:25,455 --> 00:03:29,037
Los tiempos de crisis exigen más participación y más diversa,

74
00:03:29,602 --> 00:03:32,591
más conocimiento colectivo y soberanía tecnológica: 

75
00:03:33,422 --> 00:03:35,705
un proyecto para cada problema,

76
00:03:36,054 --> 00:03:39,070
una herramienta para cada investigación.

77
00:03:40,682 --> 00:03:42,442
Abre tu kit de herramientas,

78
00:03:42,442 --> 00:03:46,694
únete a la comunidad Global de hardware científico abierto. 

