1
00:00:01,208 --> 00:00:04,194
Crisis climática, contaminación,  sequías, 

2
00:00:04,194 --> 00:00:06,294
pandemias, pobreza,  

3
00:00:06,535 --> 00:00:07,712
desigualdad,

4
00:00:07,712 --> 00:00:09,452
pérdida de biodiversidad.

5
00:00:09,452 --> 00:00:13,953
El siglo XXI está lleno de desafíos para la humanidad.

6
00:00:15,467 --> 00:00:17,143
Necesitamos más conocimiento

7
00:00:17,143 --> 00:00:19,519
y para producirlo necesitamos más ciencia.

8
00:00:20,067 --> 00:00:21,554
Pero ¿ciencia hecha dónde,

9
00:00:21,992 --> 00:00:22,775
por quiénes,

10
00:00:22,775 --> 00:00:24,282
y siguiendo qué intereses?

11
00:00:24,893 --> 00:00:26,540
Un estudio mundial reciente 

12
00:00:26,540 --> 00:00:29,928
muestra que el 90 por ciento del nuevo conocimiento científico

13
00:00:30,425 --> 00:00:32,678
se produce en países desarrollados.

14
00:00:33,276 --> 00:00:34,717
Y menos del 40

15
00:00:35,303 --> 00:00:37,631
 se dedica a resolver problemas consagrados

16
00:00:37,631 --> 00:00:40,869
en los Objetivos de Desarrollo Sostenible de las Naciones Unidas,

17
00:00:40,869 --> 00:00:42,958
como el hambre, la falta de agua potable

18
00:00:42,958 --> 00:00:44,493
o de energía accesible.

19
00:00:45,586 --> 00:00:47,218
Enfrentamos un dilema: 

20
00:00:47,218 --> 00:00:49,509
la mayor parte de las investigaciones

21
00:00:49,509 --> 00:00:52,340
no están dirigidas a los desafíos más urgentes,

22
00:00:52,340 --> 00:00:55,528
y quienes buscan usar la ciencia para abordar estos desafíos

23
00:00:55,528 --> 00:00:57,528
enfrentan barreras. 

24
00:01:00,000 --> 00:01:02,496
Necesitamos democratizar la ciencia.

25
00:01:02,496 --> 00:01:05,925
Pero la investigación científica requiere herramientas 

26
00:01:05,925 --> 00:01:07,925
cada vez más complejas:

27
00:01:07,925 --> 00:01:10,302
microscopios, reactivos químicos, 

28
00:01:10,302 --> 00:01:12,069
monitores ambientales,

29
00:01:12,069 --> 00:01:15,391
drones, computadoras y muchos otros instrumentos. 

30
00:01:17,233 --> 00:01:20,214
Suelen ser caros, difíciles de encontrar y de mantener. 

31
00:01:21,989 --> 00:01:23,042
Con esto en mente,

32
00:01:23,042 --> 00:01:24,921
un creciente movimiento global

33
00:01:24,921 --> 00:01:27,307
trabaja para abrir el equipamiento científico: 

34
00:01:27,307 --> 00:01:31,013
es decir, para compartir el conocimiento que ya existe.

35
00:01:31,013 --> 00:01:32,567
El hardware abierto se basa

36
00:01:32,567 --> 00:01:35,390
en los mismos principios que el software abierto:

37
00:01:35,390 --> 00:01:38,916
licencias que permiten a cualquier persona usar,

38
00:01:38,916 --> 00:01:41,547
estudiar, modificar y compartir diseños,

39
00:01:41,547 --> 00:01:44,261
con el objetivo de fabricar las herramientas

40
00:01:44,261 --> 00:01:46,261
que necesite a un costo menor. 

41
00:01:47,712 --> 00:01:50,477
Simplificar el acceso a los instrumentos científicos

42
00:01:50,477 --> 00:01:53,638
permite que más personas participen en la ciencia

43
00:01:53,638 --> 00:01:54,645
y, por lo tanto, 

44
00:01:54,645 --> 00:01:57,910
que produzcan conocimientos más diversos y más útiles. 

45
00:01:58,914 --> 00:02:01,316
También facilita la educación científica,

46
00:02:01,316 --> 00:02:02,667
en un círculo virtuoso.

47
00:02:03,461 --> 00:02:06,482
El movimiento mundial por el hardware científico abierto

48
00:02:06,482 --> 00:02:10,395
promueve la publicación abierta de diseños con documentación completa,

49
00:02:12,640 --> 00:02:15,083
junto con materiales de apoyo como tutoriales.

50
00:02:15,663 --> 00:02:18,311
Foros, talleres y encuentros presenciales 

51
00:02:18,311 --> 00:02:21,722
también ayudan a facilitar el proceso de aprendizaje. 

52
00:02:23,782 --> 00:02:25,852
Los componentes electrónicos de bajo costo

53
00:02:25,852 --> 00:02:27,078
y la impresión 3D

54
00:02:27,078 --> 00:02:29,543
aceleran la multiplicación del hardware abierto. 

55
00:02:30,000 --> 00:02:33,252
Que casi cualquier persona puede armar las herramientas

56
00:02:33,252 --> 00:02:35,848
es algo muy útil en situaciones de emergencia,

57
00:02:35,848 --> 00:02:39,033
o cuando la última tecnología simplemente no llega.

58
00:02:39,988 --> 00:02:41,819
Además, los instrumentos abiertos 

59
00:02:41,819 --> 00:02:44,342
son más flexibles que los productos propietarios

60
00:02:44,342 --> 00:02:47,198
y, por lo tanto, ayudan a las comunidades a empoderarse

61
00:02:47,198 --> 00:02:49,401
y realizar sus propias investigaciones.

62
00:02:49,954 --> 00:02:53,065
Esto permite abrir la agenda de producción de conocimiento. 

63
00:02:54,218 --> 00:02:57,800
Los tiempos de crisis exigen más participación y más diversa,

64
00:02:57,817 --> 00:03:01,658
más conocimiento colectivo y soberanía tecnológica:  

65
00:03:02,017 --> 00:03:04,748
un proyecto para cada problema,

66
00:03:04,734 --> 00:03:07,881
una herramienta para cada investigación.

67
00:03:09,017 --> 00:03:10,727
Abre tu kit de herramientas,

68
00:03:10,734 --> 00:03:16,027
únete a la comunidad Global de hardware científico abierto.  

