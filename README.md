# OScH Animated Video

## Wiki: OScH Video Feedback for future versions

This wiki is to keep track of edits to the "What is Open Science Hardware?" Animated Video for future versions: https://gitlab.com/gosh-community/osch-animated-video/-/wikis/OScH-Video-Feedback-for-future-versions

## Videos available on Youtube and Internet Archive
- https://www.youtube.com/watch?v=BCHgyN8d5Nk&list=PLmiNrnssuo_TW8trGW6f9sRmATsbDqvKY
- https://archive.org/details/@goshcommunity

## .SRT files for subtitles
Shorter versions
- [English](https://gitlab.com/gosh-community/osch-animated-video/-/blob/main/Animated_OScH_Video__English_Short_Subtitles.srt)
- [Spanish](https://gitlab.com/gosh-community/osch-animated-video/-/blob/main/OScH_animated_video_captions_Spanish.srt)
- [French](https://gitlab.com/gosh-community/osch-animated-video/-/blob/main/OscH_Animated_Video_French_Subtitles.srt)

Extended versions
- [English](https://gitlab.com/gosh-community/osch-animated-video/-/blob/main/OScH_Animated_Video_English_Extended.srt)
- [Spanish ](https://gitlab.com/gosh-community/osch-animated-video/-/blob/main/OScH_Animated_Video_Spanish_Extended_Subtitles.srt)
- [French](https://gitlab.com/gosh-community/osch-animated-video/-/blob/main/OScH_ANimated_video_french_extended_subtitles.srt)
