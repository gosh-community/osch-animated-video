1
00:00:01,001 --> 00:00:02,836
The climate crisis,

2
00:00:02,903 --> 00:00:04,704
pollution, droughts,

3
00:00:04,704 --> 00:00:05,671
pandemics,

4
00:00:06,072 --> 00:00:08,140
poverty, inequality

5
00:00:08,140 --> 00:00:10,242
loss of biodiversity...

6
00:00:10,242 --> 00:00:15,127
the 21st century has plenty of challenges for humanity.

7
00:00:15,593 --> 00:00:17,675
We need more knowledge,

8
00:00:17,675 --> 00:00:18,657
and to produce it,

9
00:00:18,657 --> 00:00:20,504
we need more science.

10
00:00:20,504 --> 00:00:22,244
But science done where,

11
00:00:22,244 --> 00:00:23,180
by whom,

12
00:00:23,180 --> 00:00:25,300
and following which interests?

13
00:00:25,495 --> 00:00:28,428
A recent global study shows that 90 percent 

14
00:00:28,428 --> 00:00:30,428
of new scientific knowledge

15
00:00:30,428 --> 00:00:32,861
is produced in  developed countries, 

16
00:00:32,948 --> 00:00:35,122
and less than 40 percent

17
00:00:35,122 --> 00:00:37,339
is dedicated to solving problems 

18
00:00:37,339 --> 00:00:41,094
enshrined in the UN Sustainable Development Goals -

19
00:00:41,094 --> 00:00:46,147
such as hunger, lack of clean water or affordable energy.

20
00:00:46,907 --> 00:00:48,745
We face a dilemma:

21
00:00:48,745 --> 00:00:50,962
The majority of scientific outputs

22
00:00:50,962 --> 00:00:54,697
are not directed towards the most urgent challenges,

23
00:00:54,697 --> 00:00:59,939
and those seeking to use science to address these challenges face barriers.

24
00:01:00,189 --> 00:01:03,325
We need to democratize science. 

25
00:01:03,325 --> 00:01:07,462
But scientific research requires increasingly complex tools: 

26
00:01:07,690 --> 00:01:10,458
like microscopes, chemical reagents,

27
00:01:10,458 --> 00:01:12,417
environmental monitors,

28
00:01:12,417 --> 00:01:14,222
drones, computers,

29
00:01:14,222 --> 00:01:16,873
and many other precision instruments.

30
00:01:16,873 --> 00:01:18,690
They are often expensive 

31
00:01:18,690 --> 00:01:20,777
and hard to find and maintain. 

32
00:01:21,286 --> 00:01:22,587
With this in mind,

33
00:01:22,587 --> 00:01:24,569
a growing global movement 

34
00:01:24,569 --> 00:01:27,607
is working to open up scientific equipment:

35
00:01:27,607 --> 00:01:30,859
that is, to share the knowledge already created.

36
00:01:30,946 --> 00:01:35,598
Open hardware is based on the same principles as open software:

37
00:01:35,598 --> 00:01:38,997
open licenses allowing everyone to use,

38
00:01:38,997 --> 00:01:42,125
study, modify, and share designs,

39
00:01:42,125 --> 00:01:46,096
aiming to make the tools they need at a lower cost.  

40
00:01:47,278 --> 00:01:50,112
Simplifying access to scientific instruments

41
00:01:50,112 --> 00:01:53,313
enables more people to participate in science,

42
00:01:53,313 --> 00:01:56,906
and thus produce more diverse and useful knowledge. 

43
00:01:56,906 --> 00:02:00,967
It also facilitates science education, in a virtuous circle.

44
00:02:02,502 --> 00:02:06,188
To this end, the global open science hardware movement

45
00:02:06,188 --> 00:02:10,971
promotes the open publication of designs with complete documentation,

46
00:02:10,971 --> 00:02:14,758
along with support materials such as tutorials.

47
00:02:15,149 --> 00:02:18,462
Forums, workshops and face-to-face meetings

48
00:02:18,462 --> 00:02:22,392
also help ease the learning process of assembling the tools.

49
00:02:22,728 --> 00:02:26,251
Low-cost electronic components and 3D printing

50
00:02:26,251 --> 00:02:29,432
accelerate the multiplication of open hardware. 

51
00:02:30,083 --> 00:02:32,343
Easy to assemble, DIY tools

52
00:02:32,343 --> 00:02:38,386
are very useful in emergencies or in places where the latest technology is not available.

53
00:02:38,733 --> 00:02:44,100
In addition, open instruments are more flexible than proprietary products,

54
00:02:44,100 --> 00:02:50,168
and thus allow communities to empower themselves and carry out their own research:

55
00:02:50,613 --> 00:02:54,227
they open up the agenda of knowledge production.

56
00:02:55,008 --> 00:02:59,241
Times of crisis call for greater and more diverse participation,

57
00:02:59,241 --> 00:03:03,139
collective knowledge and technological sovereignty:

58
00:03:04,050 --> 00:03:06,674
a project for every problem,

59
00:03:06,674 --> 00:03:09,119
a tool for every research team.

60
00:03:09,509 --> 00:03:10,904
Open your toolkit, 

61
00:03:10,904 --> 00:03:14,785
and join the Global Open Science Hardware community.

